<?php

/**
 * This example shows the manual way.
 */

use BitAndBlack\ImageBlur\ColorExtract;
use BitAndBlack\ImageBlur\CssCustomPropertiesFormatter;

require_once '../vendor/autoload.php';

$colorExtract = new ColorExtract('shayna-douglas-CQvFD9HrDyY-unsplash.jpg');

var_dump($colorExtract->getColors());

$cssCustomPropertiesFormatter = new CssCustomPropertiesFormatter($colorExtract);

var_dump($cssCustomPropertiesFormatter->getFormattedColors());
