<?php

/**
 * This example shows the easy way. The `example1.html` file holds the same code without the PHP part.
 */

use BitAndBlack\ImageBlur\AutoCSSImageBackground;

require_once '../vendor/autoload.php';

?>
<html lang="en">
    <head>
        <link rel="stylesheet" href="../src/background-blur.css">
        <title>Background Blur Example 1</title>
        <style>
            body {
                font-family: sans-serif;
            }
        </style>
    </head>
    <body>
        <h1>Image blur</h1>
        <h2>Complete usage:</h2>
        <div class="background-blur" style="<?php echo new AutoCSSImageBackground('shayna-douglas-CQvFD9HrDyY-unsplash.jpg'); ?>">
            <img src="shayna-douglas-CQvFD9HrDyY-unsplash.jpg" width="640" height="960" alt="Test image">
        </div>
        <h2>Compare:</h2>
        <table>
            <tr>
                <td>
                    <img src="shayna-douglas-CQvFD9HrDyY-unsplash.jpg" width="320" height="480" alt="Test image">
                </td>
                <td>
                    <div class="background-blur" style="width: 320px; height: 480px; <?php echo new AutoCSSImageBackground('shayna-douglas-CQvFD9HrDyY-unsplash.jpg'); ?>"></div>
                </td>
            </tr>
        </table>
    </body>
</html>
