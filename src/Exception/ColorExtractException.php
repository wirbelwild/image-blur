<?php

/**
 * Bit&Black Image Blur.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageBlur\Exception;

use BitAndBlack\ImageBlur\Exception;

/**
 * Class ColorExtractException.
 */
class ColorExtractException extends Exception
{
    /**
     * @param string $file
     */
    public function __construct(string $file)
    {
        parent::__construct(
            'Color values from image "' . $file . '" could not be extracted.'
        );
    }
}
