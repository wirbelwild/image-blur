<?php

namespace BitAndBlack\ImageBlur\Exception;

use BitAndBlack\ImageBlur\Exception;

/**
 * Class FileNotFoundException.
 */
class FileNotFoundException extends Exception
{
    /**
     * @param string $file
     */
    public function __construct(string $file)
    {
        parent::__construct('File "' . $file . '" does not exist.');
    }
}
