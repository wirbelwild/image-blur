<?php

/**
 * Bit&Black Image Blur.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageBlur;

use Stringable;

/**
 * The AutoCSSImageBackground class takes an image, extracts its colors and creates custom CSS properties out of them.
 */
class AutoCSSImageBackground implements Stringable
{
    private string $values;

    public function __construct(string $image)
    {
        try {
            $colorExtract = new ColorExtract($image);
            $cssCustomPropertiesFormatter = new CssCustomPropertiesFormatter($colorExtract);
        } catch (Exception) {
            $this->values = '';
            return;
        }

        $this->values = $cssCustomPropertiesFormatter->getFormattedColors();
    }

    public function __toString(): string
    {
        return $this->values;
    }
}
