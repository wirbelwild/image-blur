<?php

/**
 * Bit&Black Image Blur.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageBlur;

use BitAndBlack\ImageBlur\Exception\ColorExtractException;
use BitAndBlack\ImageBlur\Exception\FileNotFoundException;
use Color\Exception;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\RGB;
use Imagick;
use ImagickException;
use ImagickPixelException;

/**
 * The ColorExtract class extracts color data from a pixel in a given image file.
 *
 * @see \BitAndBlack\ImageBlur\Tests\ColorExtractTest
 */
class ColorExtract
{
    private readonly string $file;

    private int $downscalePercent = 10;

    private int $paddingPercent = 25;

    private bool $isExtractedCalled = false;

    /**
     * @var array<string, RGB>
     */
    private array $colors = [];

    /**
     * @throws FileNotFoundException
     */
    public function __construct(string $file)
    {
        if (!file_exists($file)) {
            throw new FileNotFoundException($file);
        }
        
        $this->file = $file;
    }

    /**
     * The value indicates the percentage to which the image should be reduced. This enables faster color extraction.
     * At the same time, the image must not be scaled too small so that characteristic color values are retained.
     *
     * @return $this
     */
    public function setDownscalePercent(int $downscalePercent): self
    {
        $this->downscalePercent = $downscalePercent;
        return $this;
    }

    /**
     * The distance from the image border in percent where the colors get extracted from.
     * This can improve extracting the color values, because the colors with a little distance from the
     * outer image border are more recognizable.
     *
     * @return $this
     */
    public function setPaddingPercent(int $paddingPercent): self
    {
        $this->paddingPercent = $paddingPercent;
        return $this;
    }

    /**
     * Returns an array of color values.
     *
     * @return array<string, RGB>
     * @throws ColorExtractException
     */
    public function getColors(): array
    {
        if (true !== $this->isExtractedCalled) {
            try {
                $this->extract();
            } catch (ImagickException | ImagickPixelException | Exception) {
                throw new ColorExtractException($this->file);
            }
        }
        
        return $this->colors;
    }

    /**
     * Extracts the color data.
     *
     * @throws InvalidInputNumberException
     * @throws ImagickException
     * @throws ImagickPixelException
     */
    private function extract(): void
    {
        $downscale = $this->downscalePercent / 100;
        $padding = $this->paddingPercent / 100;
        
        $imagick = new Imagick($this->file);

        $width = $imagick->getImageWidth();
        $height = $imagick->getImageHeight();
        
        $newWidth = (int) round($width * $downscale);
        $newHeight = (int) round($height * $downscale);
        
        $imagick->resizeImage($newWidth, $newHeight, Imagick::FILTER_GAUSSIAN, 1);
        $imagick->blurImage(0, 15);

        $pixelPositions = [
            'color-top-left' => [
                round($newWidth * $padding),
                round($newHeight * $padding),
            ],
            'color-top-right' => [
                $newWidth,
                round($newHeight * $padding),
            ],
            'color-bottom-left' => [
                round($newWidth * $padding),
                $newHeight,
            ],
            'color-bottom-right' => [
                $newWidth,
                $newHeight,
            ],
            'color-center' => [
                round($newWidth / 2),
                round($newHeight / 2),
            ],
        ];

        foreach ($pixelPositions as $name => $position) {
            $pixel = $imagick->getImagePixelColor((int) $position[0], (int) $position[1]);
            $color = $pixel->getColor();
            $rgb = new RGB((int) $color['r'], (int) $color['g'], (int) $color['b']);
            $this->colors[$name] = $rgb;
        }

        $this->isExtractedCalled = true;
    }
}
