<?php

/**
 * Bit&Black Image Blur.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageBlur;

use BitAndBlack\ImageBlur\Exception\ColorExtractException;
use Color\Value\RGB;
use Stringable;

/**
 * Formats the extracted colors to a String of Css custom properties.
 *
 * @see \BitAndBlack\ImageBlur\Tests\CssCustomPropertiesFormatterTest
 */
readonly class CssCustomPropertiesFormatter implements Stringable
{
    /**
     * @var array<string, RGB>
     */
    private array $colors;

    /**
     * @throws ColorExtractException
     */
    public function __construct(ColorExtract $colorExtract)
    {
        $this->colors = $colorExtract->getColors();
    }

    public function __toString(): string
    {
        return $this->getFormattedColors();
    }

    public function getFormattedColors(): string
    {
        $colors = [];

        foreach ($this->colors as $name => $color) {
            $formattedValue = $color->getFormattedValue('%s, %s, %s');
            $colors[$name] = '--' . $name . ': ' . $formattedValue . ';';
        }

        return implode(' ', $colors);
    }
}
