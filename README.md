[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/image-blur)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/image-blur/v/stable)](https://packagist.org/packages/bitandblack/image-blur)
[![Total Downloads](https://poser.pugx.org/bitandblack/image-blur/downloads)](https://packagist.org/packages/bitandblack/image-blur)
[![License](https://poser.pugx.org/bitandblack/image-blur/license)](https://packagist.org/packages/bitandblack/image-blur)

# Bit&Black Image Blur

Extract the most important color values from an image and create CSS gradients out of them. The color gradients can be used to display a simple blurred preview of an image while it still loads.

The **Bit&Black Image Blur** library is a CSS-only alternative to the [BlurHash](https://blurha.sh) algorithm.

| Original image                                                                                           | Blurred version with CSS gradients                                                                       |
|----------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| ![The original image](https://bitbucket.org/wirbelwild/image-blur/raw/master/example/image-original.jpg) | ![The blurred version](https://bitbucket.org/wirbelwild/image-blur/raw/master/example/image-blurred.jpg) |

## Usage

The `AutoCSSImageBackground` class takes an image and returns the color values as CSS custom properties. To get the CSS gradient working, you need to add the CSS class `background-blur` to the HTML. All together it may look like that: 

````php
<?php 

use BitAndBlack\ImageBlur\AutoCSSImageBackground;

echo '<img src="my-image.jpg" class="background-blur" style="' . new AutoCSSImageBackground('my-image.jpg') . '">';
````

And the output will be similar to:

````html
<img src="my-image.jpg" class="background-blur" style="--color-top-left: rgb(181, 126, 116); --color-top-right: rgb(202, 181, 115); --color-bottom-left: rgb(183, 206, 213); --color-bottom-right: rgb(181, 203, 206); --color-center: rgb(149, 159, 143);">
````

The CSS class `background-blur` can be found in the `src` folder. There is a `scss` file with a mixin too, where you can define the class name and the default color values.

-   [`background-blur.scss`](./src/background-blur.scss)
-   [`background-blur.css`](./src/background-blur.css)

### The manual way

Initialize the `ColorExtract` class with your image at first:

```php
<?php 

use BitAndBlack\ImageBlur\ColorExtract;

$colorExtract = new ColorExtract('my-image.jpg');
```

Get the color values by calling the `getColors` method then:

```php
<?php 

$colors = $colorExtract->getColors();
```

#### Options

The method `setDownscalePercent` can be used to specify the percentage to which the image should be reduced. This enables faster color extraction. At the same time, the image must not be scaled too small so that characteristic color values are retained. For example:

```php
<?php 

$colorExtract->setDownscalePercent(10);
```

The method `setPaddingPercent` can be used to specify the distance from the image border in percent where the colors get extracted from. This can improve extracting the color values, because the colors with a little distance from the outer image border are more recognizable. For example:me

```php
<?php 

$colorExtract->setPaddingPercent(25);
```

#### Formatting

If you want to format the colors into CSS custom properties, you can use the `CssCustomPropertiesFormatter` class:

```php
<?php 

use BitAndBlack\ImageBlur\CssCustomPropertiesFormatter;

$formattedColors = CssCustomPropertiesFormatter($colorExtract);
```

But for sure you can use your own custom formatter too.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
