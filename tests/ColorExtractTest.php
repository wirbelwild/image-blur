<?php

/**
 * Bit&Black Image Blur.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageBlur\Tests;

use BitAndBlack\ImageBlur\ColorExtract;
use BitAndBlack\ImageBlur\Exception\ColorExtractException;
use BitAndBlack\ImageBlur\Exception\FileNotFoundException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\RGB;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ColorExtractTest extends TestCase
{
    /**
     * @throws ColorExtractException
     * @throws FileNotFoundException
     * @throws InvalidInputNumberException
     */
    #[DataProvider('getImages')]
    public function testCanExtractColor(string $file): void
    {
        $expectedOutput = [
            'color-top-left' => new RGB(134, 127, 57),
            'color-top-right' => new RGB(114, 128, 67),
            'color-bottom-left' => new RGB(134, 128, 67),
            'color-bottom-right' => new RGB(114, 126, 78),
            'color-center' => new RGB(121, 127, 71),
        ];
        
        $colorExtract = new ColorExtract($file);
        $colors = $colorExtract->getColors();
        
        self::assertEquals(
            $expectedOutput,
            $colors
        );
    }

    public function testThrowsExceptionOnMissingImage(): void
    {
        $this->expectException(FileNotFoundException::class);
        new ColorExtract('missing.jpg');
    }

    /**
     * @throws FileNotFoundException
     */
    public function testThrowsExceptionOnWrongFile(): void
    {
        $this->expectException(ColorExtractException::class);
        $colorExtract = new ColorExtract(__DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'no-image.txt');
        $colorExtract->getColors();
    }

    /**
     * @return Generator
     */
    public static function getImages(): Generator
    {
        $imageDir = __DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

        yield [$imageDir . 'RGGB.gif'];
        yield [$imageDir . 'RGGB.jpg'];
        yield [$imageDir . 'RGGB.png'];
    }
}
