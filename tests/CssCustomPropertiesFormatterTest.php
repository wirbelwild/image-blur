<?php

/**
 * Bit&Black Image Blur.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageBlur\Tests;

use BitAndBlack\ImageBlur\ColorExtract;
use BitAndBlack\ImageBlur\CssCustomPropertiesFormatter;
use BitAndBlack\ImageBlur\Exception\ColorExtractException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\RGB;
use PHPUnit\Framework\TestCase;

class CssCustomPropertiesFormatterTest extends TestCase
{
    /**
     * @throws ColorExtractException
     * @throws InvalidInputNumberException
     */
    public function testCanFormatColors(): void
    {
        $input = [
            'color-top-left' => new RGB(0, 1, 2),
            'color-top-right' => new RGB(3, 4, 5),
        ];

        $colorExtract = $this->createMock(ColorExtract::class);
        $colorExtract
            ->method('getColors')
            ->willReturn($input)
        ;

        $formatter = new CssCustomPropertiesFormatter($colorExtract);

        self::assertSame(
            '--color-top-left: 0, 1, 2; --color-top-right: 3, 4, 5;',
            (string) $formatter
        );
    }
}
